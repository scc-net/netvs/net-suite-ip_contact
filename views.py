from . import ip_contact, METADATA
from net_suite.model import *
from flask_breadcrumbs import register_breadcrumb
from net_suite import app
from net_suite.views import login_required, get_db_conn, db
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Length, HostnameValidation
from flask import request, flash, redirect, url_for
import smtplib
from email.mime.text import MIMEText


@ip_contact.route('/', methods=['GET', 'POST'])
@register_breadcrumb(ip_contact, '.', METADATA.printable_name)
@login_required
def main():
    request_form = ContactForm(request.form)
    if request.method == 'POST':
        if request_form.validate_on_submit():
            s = request.environ['beaker.session']['login']
            net_str = request_form.net.data.strip()
            areas = DBNetArea.get_areas_by_subnet_sorted_by_netsize(db, get_db_conn(), net_str)
            target_area = None
            if len(areas) > 1:
                flash('Netz nicht eindeutig zuordenbar. Ggf. muss ein kleineres Netz angeben werden.', category='danger')
            elif len(areas) == 0:
                flash('Netz nicht bekannt.', category='danger')
            else:
                target_area = areas[0]
            if target_area is not None:
                msg = MIMEText("""
Diese Nachricht stammt aus dem Formular "{form_name}" des Netzdiensteportals des SCCs (NETVS).
Durch Antworten auf diese E-Mail antworten Sie direkt dem Absender des Formulars.

Angefragtes Netz/IP: {net}
Nachricht:

{msg}
                """.format(form_name=METADATA.printable_name, net=net_str, msg=request_form.message.data).encode('utf-8'), 'plain', 'utf-8')
                msg['Subject'] = 'DNSVS: Addressbereich "' + target_area.print_name + '"'
                msg['From'] = app.config.get('IP_CONTACT_FROM_ADDR')
                msg['Reply-To'] = s.email
                if not db.host_omdl.OP_ENV_IS_PROD:
                    msg['To'] = s.email
                else:
                    mgrs = target_area.get_mgrs(db=db, connection=get_db_conn())
                    if len(mgrs) == 0:
                        msg['To'] = METADATA.contact_mail
                    else:
                        msg['To'] = ','.join([m.email for m in mgrs])

                s = smtplib.SMTP(app.config.get('IP_CONTACT_SMARTHOST'))
                s.send_message(msg)
                s.quit()
                flash("Nachricht erfolgreich an die Betreuer der angegebenen Adresse/des angegbenen Subnetzes gesendet. "
                      "Die Betreuer werden Sie gegebenenfalls per Mail kontaktieren.", category="success")
                return redirect(url_for('ip_contact.main'))

    return render_template('ip_contact/main.html', title='Adressbetreuer kontaktieren', request_form=request_form)


class ContactForm(FlaskForm):
    net = StringField('net', validators=[DataRequired()])
    message = StringField('message')
